package info.hccis.canes.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;

public class Util {

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     * @since 20180116
     * @author BJM/CIS2250
     * @param activity
     * @param title
     * @param message
     */

    public static void showDialog(final Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(activity, "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(activity, "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }



}
