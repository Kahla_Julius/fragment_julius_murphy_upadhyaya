package info.hccis.canes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.view.Menu;
import android.widget.Toast;

import info.hccis.canes.bo.Camper;
import info.hccis.canes.bo.CamperContent;
import info.hccis.canes.dao.MyAppDatabase;
import info.hccis.canes.ui.KahlaFragment;
import info.hccis.canes.ui.KapilFragment;

import info.hccis.canes.ui.MacLeanFragment;
import info.hccis.canes.ui.MurphyMacKenzieFragment;
import info.hccis.canes.util.Util;
import info.hccis.canes.util.UtilRest;

public class MainActivity extends AppCompatActivity implements CampersFragment.OnListFragmentInteractionListener,
                        MacLeanFragment.OnFragmentInteractionListener, KapilFragment.OnFragmentInteractionListener, KahlaFragment.OnFragmentInteractionListener, MurphyMacKenzieFragment.OnFragmentInteractionListener {

    private AppBarConfiguration mAppBarConfiguration;
    public static MyAppDatabase myAppDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send, R.id.nav_campers_list, R.id.nav_maclean, R.id.nav_kapil, R.id.nav_kahla, R.id.nav_murphy)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        myAppDatabase = Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class,"canesdb").allowMainThreadQueries().build();

        //Test the Room functionality
        CamperContent.getCampersFromRoom();


        //******************************************************************************************
        // BJM 20200117
        // Call the method which will load the campers list associated with the recyclerView.
        // Want to set this up so the list will be loaded when the use navigates to the recyclerview.
        //******************************************************************************************
        CamperContent.loadCampers();




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onListFragmentInteraction(Camper item) {
        Log.d("bjm", "item communicated from fragment: "+item.toString());
    }


    @Override
    public void onFragmentInteraction(String message) {
        Log.d("bjm","Hello "+message);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
